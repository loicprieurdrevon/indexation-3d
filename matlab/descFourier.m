classdef descFourier
    %DESCFOURIER Descripteur de forme de Fourier
    %   calcule le contour de la forme, et retourne
    %   sa transform�e de Fourier normalis�e
    
    properties (Constant = true)
        nbPoints = 128; % nombre de points du contour
        descSize = 16; % fr�quences du spectre � conserver
    end
    
    properties
       values; % spectre du contour (taille 'nbFreq') 
    end
    
    methods
         % constructeur (� partir d'une image blanche sur noire)
         function dst = descFourier(shape)
            % take leftmost point of object, intersecting medium hight of image
            dim = size(shape);
            
            l = find(shape, 1, 'last')-1;
            col = 1+floor(l/dim(1));
            row = 1+mod(l, dim(1));
            disp([col, row]);
            %compute boundary
            F = bwtraceboundary(shape,[row, col],'NE', 8, Inf, 'counterclockwise');
            points = size(F);
            %disp(points);
            %scatter(F(:,1), F(:,2));
            %disp(points);
            %disp(is*(points(1)));
            Sample = interp1(1:1:points(1) ,F(:,1)+1i*F(:,2), 1:points(1)/128.0:points(1), 'cubic',  'extrap' );
            disp(points(1));
            
            %17 first terms
            fourier = fft(Sample,descFourier.descSize+1);
            
            %remove first term:
            fourier = fourier(2:end);
            
            %compute modules
            modules = abs(fourier);
            
            %normalize
            modules = modules/modules(1);
            
            
            dst.values = modules;
            
            
         end
         
         % distance entre deux descripteurs
        function d = distance(desc1, desc2)
           
            d = mean(abs(desc1.values - desc2.values));
            disp(d);
        end
    end
    
end
